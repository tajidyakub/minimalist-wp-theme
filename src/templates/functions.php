<?php
/**
 * Template's functions file.
 * Automatically availble in template file;
 *
 * @package minimalist
 * @since 1.0.0
 */

// TODO: Web and App Icons + Manifest file

// Themes setup hooked to wordpress
add_action('after_setup_theme', 'minimalist_setup');

// Themes setup function
// - Theme's support
// - Post type support
// - Post thumbnails
// - Thumbail sizes
// - Register menus
// - Custom mod for logos
// - Internationalization (textdomain)

function minimalist_setup()
{
  add_theme_support('title-tag');
  add_theme_support('automatic-feed-links');
  add_theme_support('post-thumbnails');

  // Custom background feature
  add_theme_support(
    'custom-background',
    apply_filters(' minimalist_custom_bg_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    ))
  );

  // Custom Logo
  add_theme_support('custom-logo', array(
    'height' => 32,
    'width' => 250,
    'flex-width' => true,
    'flex-height' => true,
  ));

  // Switch default core markup for search form, comment form, and comments
  // to output valid HTML5.
  add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));
  // Register Nav Menu Locations
  register_nav_menus(array(
    'navbar_top' => __('Navigation Bar', 'minimalist'),
    'footer' => __('Footer Menu', 'minimalist'),
  ));

  // Internationalization
  load_theme_textdomain('minimalist', get_template_directory() . '/langs');
  add_post_type_support('page', 'excerpt');
  // Add image sizes
  add_image_size('large', 1280, 768, true);
  add_image_size('medium', 1024, 576, true);
  add_image_size('thumbnail', 240, 135, true);
}

// Register image sizes tobe available in Add Media modal
add_filter('image_size_names_choose', 'minimalist_custom_sizes');

// Image Sizes function callback
function minimalist_custom_sizes($sizes)
{
  return array_merge($sizes, array(
    'large' => __('Large Size'),
    'medium' => __('Medium Size'),
    'thumbnail' => __('Thumbnail Size'),
  ));
}

add_action('widgets_init', 'minimalist_widgets_init');

// Register widget area.
function minimalist_widgets_init()
{
  register_sidebar(array(
    'name' => esc_html__('Sidebar', 'boilerplate'),
    'id' => 'sidebar-1',
    'description' => esc_html__('Add widgets here.', 'boilerplate'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '<small><i class="fab fa-hotjar"></i></small></h2>',
  ));
  register_sidebar(array(
    'name' => esc_html__('Footer', 'boilerplate'),
    'id' => 'footer-1',
    'description' => esc_html__('Add widgets here.', 'boilerplate'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>',
  ));
}

add_action('wp_enqueue_scripts', 'minimalist_enqueue');
// Enqueue Styles and Scripts
function minimalist_enqueue()
{
  wp_enqueue_style(
    'style/main',
    get_stylesheet_uri(),
    array(),
    filemtime(get_template_directory() . '/style.css'),
    'all'
  );
  wp_enqueue_style(
    'style/fontawesome',
    get_template_directory_uri() . '/css/fontawesome.min.css',
    array(),
    '5.5.0',
    'all'
  );
  // wp_enqueue_script( 'bp/js/type', get_template_directory_uri() . '/js/type.js' , array(), filemtime( get_template_directory() . '/js/typew.js'), false);
  wp_enqueue_script(
    'scripts/app',
    get_template_directory_uri() . '/scripts/app.js',
    array(),
    filemtime(get_template_directory() . '/scripts/app.js'),
    true
  );
}

/**
 * Hook for displaying subtitle.
 *
 * Create an action to display to content of the subtitle
 * custom field which requires a post id.
 *
 * @param integer $post_id ID of the post contains subtitle custom field
 * @return void
 *
 * Example use, inside loop.
 * do_action('the_subtitle', get_the_ID());
 */

// TODO: better placement will be in site plugin.

add_action('the_subtitle', 'the_subtitle');

function get_the_subtitle($post_id)
{
  $key = 'subtitle';
  $single = true;
  return get_post_meta($post_id, $key, $single);
}
function the_subtitle($post_id)
{
  echo get_the_subtitle($post_id);
}

// Web headers.
// Display custom logo is already defined in the customizer.

add_action('the_web_header', 'the_web_header');

function get_the_web_header()
{
  $logo_id = get_theme_mod('custom_logo');
  $logo = wp_get_attachment_image_src($logo_id, 'full');
  $web_title = get_bloginfo('name');
  if (has_custom_logo) {
    return '<a href="/" title="' .
      __('Back to home', 'boilerplate') .
      '"><img src=". $logo[0] ." alt="' .
      $web_title .
      '" /></a>';
  } else {
    return '<h1><a href="/" title="' .
      __('Back to home', 'boilerplate') .
      '">' .
      $web_title .
      '</h1></a>';
  }
}

function the_web_header()
{
  echo get_the_web_header();
}

/**
 * Site options page. Created with ACF.
 */
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title' => 'Site Configuration',
    'menu_title' => 'Site Settings',
    'menu_slug' => 'site-settings',
    'capability' => 'edit_posts',
    'redirect' => true,
  ));

  acf_add_options_sub_page(array(
    'page_title' => 'Additional Contents',
    'menu_title' => 'Contents',
    'parent_slug' => 'site-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title' => 'Additional Themes Settings',
    'menu_title' => 'Themes',
    'parent_slug' => 'site-settings',
  ));
}

/**
 * Add Manifest file and Icons in the html head.
 *
 * Additional meta tag / informations to be put inside html head
 * Hooked to wp_head action
 *
 * @param none
 * @return void
 */

if (!function_exists('add_html_head')) {
  add_action('wp_head', 'add_html_head');

  function add_html_head()
  {
    // Array of additional tags
    // TODO: Add metas using ACF

    $icns_uri = get_template_directory_uri() . '/icns';
    $head_tags = [];
    $head_tags[] = array(
      'tag_name' => 'meta',
      'tag_value' => array('name' => 'google', 'content' => 'notranslate'),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '57x57',
        'href' => $icns_uri . '/apple-icon-57x57.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '60x60',
        'href' => $icns_uri . '/apple-icon-60x60.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '72x72',
        'href' => $icns_uri . '/apple-icon-72x72.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '76x76',
        'href' => $icns_uri . '/apple-icon-76x76.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '114x114',
        'href' => $icns_uri . '/apple-icon-114x114.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '120x120',
        'href' => $icns_uri . '/apple-icon-120x120.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '144x144',
        'href' => $icns_uri . '/apple-icon-144x144.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '152x152',
        'href' => $icns_uri . '/apple-icon-152x152.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'apple-touch-icon',
        'sizes' => '180x180',
        'href' => $icns_uri . '/apple-icon-180x180.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'icon',
        'type' => 'image/png',
        'sizes' => '192x192',
        'href' => $icns_uri . '/android-icon-192x192.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'icon',
        'type' => 'image/png',
        'sizes' => '32x32',
        'href' => $icns_uri . '/favicon-32x32.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'icon',
        'type' => 'image/png',
        'sizes' => '96x96',
        'href' => $icns_uri . '/favicon-96x96.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array(
        'rel' => 'icon',
        'type' => 'image/png',
        'sizes' => '16x16',
        'href' => $icns_uri . '/favicon-16x16.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'link',
      'tag_value' => array('rel' => 'manifest', 'href' => $icns_uri . '/manifest.json'),
    );
    $head_tags[] = array(
      'tag_name' => 'meta',
      'tag_value' => array('name' => 'msapplication-TileColor', 'content' => '#ffffff'),
    );
    $head_tags[] = array(
      'tag_name' => 'meta',
      'tag_value' => array(
        'name' => 'msapplication-TileImage',
        'content' => $icns_uri . '/ms-icon-144x144.png',
      ),
    );
    $head_tags[] = array(
      'tag_name' => 'meta',
      'tag_value' => array('name' => 'theme-color', 'content' => '#212121'),
    );

    echo '<!-- Add HTML Metas -->';
    foreach ($head_tags as $tags) {
      $printed = '<' . $tags['tag_name'] . ' ';
      foreach ($tags['tag_value'] as $key => $value) {
        $printed .= $key . '="' . $value . '" ';
      }
      $printed .= ' />';
      echo $printed;
    }
    echo '<!-- End Add HTML Metas -->';
  }
}

/**
 * Remove Jetpack's Related Posts.
 */

function jetpackme_remove_rp() {
    if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
        $jprp = Jetpack_RelatedPosts::init();
        $callback = array( $jprp, 'filter_add_target_to_dom' );
        remove_filter( 'the_content', $callback, 40 );
    }
}
add_filter( 'wp', 'jetpackme_remove_rp', 20 );

/**
 * Customize Jetpack's Related Posts.
 *
 * Execute init_raw method from Jetpack Class to integrate
 * the appearance of Jetpack's Related Posts with our  Styles.
 *
 * @param array $post_id Post ID
 */

add_filter('related_posts', 'custom_related_posts');

function custom_related_posts( $post_id ) {
    // Posts array placeholder
    $posts = array();

    if ( class_exists( 'Jetpack_RelatedPosts' ) && method_exists( 'Jetpack_RelatedPosts', 'init_raw' ) ) {
        $related = Jetpack_RelatedPosts::init_raw()
            ->set_query_name( 'custom-related' ) // Optional, name can be anything
            ->get_for_post_id(
                $post_id,
                array( 'size' => 3 )
            );

        if ( $related ) {
            $i = 0;
            foreach ( $related as $result ) {
                // Get the related post IDs
                $related_post = get_post( $result[ 'id' ] );
                // From there you can do just about anything. Here we get the post titles
                $posts[$i]['ID'] = $related_post->ID;
                $posts[$i]['title'] = $related_post->post_title;
                $posts[$i]['subtitle'] = get_the_subtitle($result['id']);
                $posts[$i]['excerpt'] = $related_post->post_excerpt;
                $posts[$i]['permalink'] = get_the_permalink($related_post->ID);
                $posts[$i]['category'] = get_the_category($related_post->ID);
                $posts[$i]['date'] = get_the_date('d/m/Y');
                $posts[$i]['thumbnail'] = get_the_post_thumbnail_url( $related_post->ID, 'small' );;
                $i++;
            }
        }
    }

    return $posts;
}

// Create a [custom-related] shortcode
add_shortcode( 'custom-related', 'custom_related_posts' );

// Stop JetPacks Minified/Concatention CSS file
add_filter( 'jetpack_implode_frontend_css', '__return_false' );
//Remove JepPack CSS
function miniboot_remove_jetpack_css() {
  wp_deregister_style( 'jetpack_related-posts' ); //Related Posts
}
add_action('wp_enqueue_scripts ', 'miniboot_remove_jetpack_css' );

// Remove default Likes and Shared
add_action( 'loop_start', 'jp_remove_share' );

function jp_remove_share() {
    remove_filter( 'the_content', 'sharing_display', 19 );
    remove_filter( 'the_excerpt', 'sharing_display', 19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}

// Display post likes with custom action

add_action('show_likes', 'show_likes');
add_action('show_shares', 'show_shares');

function show_likes() {
    if ( class_exists( 'Jetpack_Likes' ) ) {
        $custom_likes = new Jetpack_Likes;
        echo $custom_likes->post_likes( '' );
    }
}

function show_shares() {
    if ( function_exists( 'sharing_display' ) ) {
        sharing_display( '', true );
    }
}

/**
 * Customize Admin footer text
 *
 * Get the custom text from Options page.
 */

function admin_footer_mod () {
    $footer_text = get_field('admin_footer_text', 'option');
    return $footer_text;
  }

add_filter('admin_footer_text', 'admin_footer_mod');
